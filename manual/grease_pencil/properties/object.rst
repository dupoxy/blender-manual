
*****************
Object Properties
*****************

.. _grease_pencil-object-visibility:

Visibility
==========

.. reference::

   :Panel:     :menuselection:`Object Properties --> Visibility`

.. _bpy.types.Object.use_grease_pencil_lights:

Use Light
   Enables the Grease Pencil object to be affected by lights.

   This property affect the whole object, for more control with lights you can enable or disable
   the use of lights by layers. See :doc:`Layers </grease_pencil/properties/layers>` for more information.

   .. figure:: /images/grease-pencil_object_use-light.png

      Lights disabled (left) and enabled (right).

.. seealso::

   There are several other :doc:`general visibility </scene_layout/object/properties/visibility>` properties.
