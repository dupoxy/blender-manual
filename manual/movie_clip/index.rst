.. _bpy.types.SpaceClipEditor:
.. _bpy.types.MovieClip:
.. _bpy.ops.clip:
.. _editors-movieclip-index:

#############################
  Motion Tracking & Masking
#############################

You perform masking and tracking with the Movie Clip Editor.

.. figure:: /images/editors_clip_introduction_example.png

   The Movie Clip Editor.

See :doc:`/editors/clip/index` for more information on the Movie Clip Editor.

.. toctree::
   :maxdepth: 2

   tracking/index.rst
   masking/index.rst
