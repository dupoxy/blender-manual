.. _bpy.types.Shader:

##########
  Shader
##########

.. toctree::
   :maxdepth: 1

   add.rst
   background.rst
   diffuse.rst
   emission.rst
   glass.rst
   glossy.rst
   hair.rst
   holdout.rst
   mix.rst
   metallic.rst
   principled.rst
   hair_principled.rst
   volume_principled.rst
   ray_portal.rst
   refraction.rst
   specular_bsdf.rst
   sss.rst
   toon.rst
   translucent.rst
   transparent.rst
   sheen.rst
   volume_absorption.rst
   volume_scatter.rst
