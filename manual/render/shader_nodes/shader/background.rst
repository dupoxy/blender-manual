.. _bpy.types.ShaderNodeBackground:

**********
Background
**********

.. figure:: /images/node-types_ShaderNodeBackground.webp
   :align: right
   :alt: Background Shader node.

The *Background* shader node is used to add background light emission.
This node should only be used for the :doc:`/render/shader_nodes/output/world`.


Inputs
======

Color
   Color of the emitted light.
Strength
   Strength of the emitted light.


Properties
==========

This node has no properties.


Outputs
=======

Background
   Standard shader output.
