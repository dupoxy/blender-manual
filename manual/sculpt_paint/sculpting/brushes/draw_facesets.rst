
**************
Draw Face Sets
**************

.. reference::

   :Mode:      Sculpt Mode
   :Brush:     :menuselection:`Sidebar --> Tool --> Brush Settings --> Advanced --> Brush Type`

Draw new or extend existing :ref:`Face Sets <sculpting-editing-facesets>` with each stroke.

Holding :kbd:`Ctrl` will continue drawing the same face set as the one under the cursor.
Holding :kbd:`Shift` will relax or smooth the edges of the face sets
by modifying the underlying topology so edges flow along the perimeter of the face sets.
This will remove the jagged lines visible after drawing or creating a face set.

.. note::

   More information in the
   :doc:`Face Set Introduction </sculpt_paint/sculpting/introduction/visibility_masking_face_sets>`.


Brush Settings
==============

General
-------

While a lot of the general brush settings are supported,
it's not needed to change them from the default,
as the brush purpose is very simple.

.. note::

   More info at :ref:`sculpt-tool-settings-brush-settings-general` brush settings
   and on :ref:`sculpt-tool-settings-brush-settings-advanced` brush settings.
