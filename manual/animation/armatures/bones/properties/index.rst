
##############
  Properties
##############

.. reference::

   :Mode:      Object Mode, Edit Mode and Pose Mode
   :Panel:     :menuselection:`Properties --> Bone`

When bones are selected (hence in *Edit Mode* and *Pose Mode*), their
properties are shown in the *Bone* tab of the Properties.
This shows different panels used to control features of each selected bone;
the panels change depending on which mode you are working in.

.. toctree::
   :maxdepth: 2

   transform.rst
   bendy_bones.rst
   relations.rst
   inverse_kinematics.rst
   deform.rst
   display.rst
   custom_properties.rst
