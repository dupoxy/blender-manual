
*************
Curve Display
*************

.. reference::

   :Mode:      Edit Mode
   :Panel:     :menuselection:`3D Viewport --> Viewport Overlays --> Curve Edit Mode`

When in Edit Mode, curves have special :doc:`overlays </editors/3dview/display/overlays>`
to control how curves are displayed in the 3D Viewport.

.. _bpy.types.View3DOverlay.display_handle:

Handles
   Controls the visibility of Bézier curve handles in edit mode.

   :None: Hides all Bézier curve handles, providing an unobstructed view of the curve.
   :Selected: Displays the handles only for selected control points.
   :All: Displays the handles for all control points in the curve.
