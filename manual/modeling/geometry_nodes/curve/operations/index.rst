
#########################
  Curve Operation Nodes
#########################

.. toctree::
   :maxdepth: 1

   curve_to_mesh.rst
   curve_to_points.rst
   curves_to_grease_pencil.rst
   deform_curves_on_surface.rst
   fill_curve.rst
   fillet_curve.rst
   grease_pencil_to_curves.rst
   interpolate_curves.rst
   merge_layers.rst
   resample_curve.rst
   reverse_curve.rst
   subdivide_curve.rst
   trim_curve.rst
