
########################
  Write Geometry Nodes
########################

.. toctree::
   :maxdepth: 1

   set_geometry_name.rst
   set_id.rst
   set_position.rst
   set_selection.rst
