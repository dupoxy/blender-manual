
############
  Generate
############

.. toctree::
   :maxdepth: 1

   array.rst
   bevel.rst
   booleans.rst
   build.rst
   decimate.rst
   edge_split.rst
   geometry_nodes.rst
   mask.rst
   mesh_to_volume.rst
   mirror.rst
   multiresolution.rst
   remesh.rst
   screw.rst
   skin.rst
   solidify.rst
   subdivision_surface.rst
   triangulate.rst
   volume_to_mesh.rst
   weld.rst
   wireframe.rst
