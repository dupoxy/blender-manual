

####################################
  Troubleshooting Windows Hardware
####################################

.. toctree::
   :maxdepth: 2

   NVIDIA <nvidia.rst>
   AMD <amd.rst>
   Intel <intel.rst>
   Other GPU <other.rst>
