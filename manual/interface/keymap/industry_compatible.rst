
**************************
Industry Compatible Keymap
**************************

While this is not a comprehensive list,
this page shows common keys used in the industry compatible keymap.


General
=======

.. list-table::
   :align: left
   :width: 95%
   :widths: 20 80

   * - :kbd:`1` - :kbd:`3`
     - Switch :doc:`Selection mode </modeling/meshes/selecting/introduction>`
   * - :kbd:`4`
     - Object Mode
   * - :kbd:`5`
     - :doc:`Modes </editors/3dview/modes>` Pie Menu
   * - :kbd:`RMB`
     - Context menu
   * - :kbd:`Tab`
     - :ref:`Menu Search <bpy.ops.wm.search_menu>`
   * - :kbd:`Shift-Tab`
     - Quick access (favorites)
   * - :kbd:`Return`
     - Rename
   * - :kbd:`Ctrl-Return`
     - Render
   * - :kbd:`Ctrl-[`
     - Toggle Toolbar
   * - :kbd:`Ctrl-]`
     - Toggle Sidebar


Common Editing Keys
===================

.. list-table::
   :align: left
   :width: 95%
   :widths: 20 80

   * - :kbd:`Backspace`
     - Delete the selected item with a confirmation dialog
   * - :kbd:`Delete`
     - Delete the selected item without a confirmation dialog
   * - :kbd:`Ctrl-D`
     - Duplicate
   * - :kbd:`P`
     - Set Parent
   * - :kbd:`B`
     - :doc:`/editors/3dview/controls/proportional_editing` (a.k.a. Soft Selection)


Viewport
========

.. list-table::
   :align: left
   :width: 95%
   :widths: 20 80

   * - :kbd:`Alt-LMB`
     - Orbit View
   * - :kbd:`Alt-MMB`
     - Pan View
   * - :kbd:`Alt-RMB`
     - Zoom View
   * - :kbd:`F1` - :kbd:`F4`
     - Front/Side/Top/Camera Viewpoints
   * - :kbd:`F`
     - Frame Selected
   * - :kbd:`Shift F`
     - Center View to Mouse
   * - :kbd:`A`
     - Frame All


Selection
=========

.. list-table::
   :align: left
   :width: 95%
   :widths: 20 80

   * - :kbd:`LMB`
     - Select
   * - :kbd:`Ctrl-A`
     - Select All
   * - :kbd:`Shift-Ctrl-A`
     - Deselect All
   * - :kbd:`Ctrl-I`
     - Select Inverse
   * - :kbd:`Up`
     - Select More
   * - :kbd:`Down`
     - Select Less
   * - Double :kbd:`LMB`
     - Select Loop
   * - Double :kbd:`Alt-LMB`
     - Select Ring
   * - :kbd:`Ctrl L`
     - Select Linked


Tools
=====

.. list-table::
   :align: left
   :width: 95%
   :widths: 20 80

   * - :kbd:`W`, :kbd:`E`, :kbd:`R`
     - Move, Rotate, Scale
   * - :kbd:`Q`
     - :doc:`Selection Tools </interface/selecting>`
   * - :kbd:`D`
     - :doc:`Annotate Tool </interface/annotate_tool>`
   * - :kbd:`C`
     - :doc:`Cursor Tool </editors/3dview/3d_cursor>`


Edit Mode Tools
===============

.. list-table::
   :align: left
   :width: 95%
   :widths: 20 80

   * - :kbd:`Ctrl-E`
     - :doc:`Extrude </modeling/meshes/editing/mesh/extrude>`
   * - :kbd:`Ctrl-B`
     - :doc:`Bevel </modeling/meshes/editing/edge/bevel>`
   * - :kbd:`I`
     - :doc:`Inset </modeling/meshes/editing/face/inset_faces>`
   * - :kbd:`K`
     - :doc:`Knife </modeling/meshes/editing/mesh/knife_topology_tool>`
   * - :kbd:`Alt-C`
     - :doc:`Loop Cut </modeling/meshes/editing/edge/loopcut_slide>`


Animation
=========

.. list-table::
   :align: left
   :width: 95%
   :widths: 20 80

   * - :kbd:`Spacebar`
     - Play/Pause
   * - :kbd:`S`
     - Set Location + Rotation + Scale keyframe
   * - :kbd:`Shift-S`
     - Insert Keyframe menu
   * - :kbd:`Shift-W`
     - Set Location Key
   * - :kbd:`Shift-E`
     - Set Rotation Key
   * - :kbd:`Shift-R`
     - Set Scale Key


Platform Specific Keys
======================

macOS
-----

The :kbd:`Cmd` key can be used instead of :kbd:`Ctrl` on macOS
for all but a few exceptions which conflict with the operating system.
