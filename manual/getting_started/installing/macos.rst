
*******************
Installing on macOS
*******************

Check the :doc:`Downloading Blender </getting_started/installing/index>` page to find the minimum requirements and the
different versions that are available for Blender (if you have not done so yet).

.. important::

   Blender supports both Intel and Apple Silicon architectures on macOS. Make sure to download a variant that is
   compatible with your CPU's architecture.


Install from a DMG
==================

Blender for macOS is distributed as disk images (dmg-files). To mount the disk image, double-click on the dmg-file.
Then drag ``Blender.app`` into the Applications folder.

Depending on the Security and Privacy preferences of your Mac, macOS will request your approval before opening Blender
for the first time.

To make the installation and configuration fully self-contained, set up a
:ref:`Portable Installation <portable-installation>`.


Updating on macOS
=================

On macOS there is one main way to update Blender. This section covers that approach.


Updating from a DMG
-------------------

When an update for Blender is released, it can be downloaded directly
from the `Blender website <https://www.blender.org/download/>`__. Install the new version by overwriting the current
``Blender.app`` in the Applications folder. You can rename ``Blender.app`` or place it in a different folder to have
more than one version at a time.

.. seealso::

   The Splash screen :doc:`/getting_started/configuration/defaults` page for information about importing settings from
   previous Blender versions and other quick settings.
