.. _about-user-contribute:

############################
  Contribute Documentation
############################

The Blender Manual is a community driven effort to which anyone can contribute. Whether you'd like to fix a tiny
spelling mistake or rewrite an entire chapter, your help with the Blender manual is most welcome!

If you find an error in the documentation, please `report the problem
<https://projects.blender.org/blender/documentation/issues/new>`__.

Get involved in discussions through any of the project `Contacts`_.

.. _about-getting-started:

.. toctree::
   :maxdepth: 2

   getting_started/index.rst
   guides/index.rst


Where to help
=============

.. toctree::
   :hidden:

   todo_list.rst
   new_features.rst

- :doc:`todo_list`
- :doc:`new_features`


.. _contribute-contact:

Contacts
========

`Project Page <https://projects.blender.org/blender/documentation>`__
   An overview of the documentation project.
`Documentation Forum <https://devtalk.blender.org/c/documentation/12>`__
   A forum based discussions on writing and translating documentation. This includes the user manual, Wiki, release
   notes, and code docs.
:ref:`blender-chat`
   ``#docs`` channel for informal discussions in real-time.
`Project Workboard <https://projects.blender.org/blender/documentation/projects>`__
   Manage tasks such as bugs, todo lists, and future plans.
