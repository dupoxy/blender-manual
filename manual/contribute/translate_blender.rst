
*****************
Translate Blender
*****************

Bring Blender closer to everyone! 🌎 By translating Blender's user interface, you help artists and creators worldwide
work in their native language. No coding skills are needed—just a passion for Blender and the ability to translate.
Join the translation community today and make Blender truly global!

For more information, see the `Blender UI Translation Guide <https://developer.blender.org/docs/handbook/translating/translator_guide/>`__.
