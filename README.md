# Blender Manual

Welcome to the Blender user manual project! We are always looking for contributors.
Whether you're a seasoned writer, a Blender expert, or just getting started, your help is valuable.

The Blender Manual is written in `reStructuredText` (RST) and built using [Sphinx](http://www.sphinx-doc.org/en/stable/).
You can view the latest version of the manual [here](https://docs.blender.org/manual/en/dev/).

If you're interested in contributing, follow the instructions below.

## ✍️ How to Build & Edit the Documentation

Before making changes, please review the style guides to ensure consistency:

- [Markup Style Guide](https://docs.blender.org/manual/en/dev/contribute/manual/guides/writing_guide.html)
- [Writing Style Guide](https://docs.blender.org/manual/en/dev/contribute/manual/guides/writing_guide.html)

Once you're familiar with the guidelines, follow these steps to get set up, edit, and submit your changes:

1. **[Install](https://docs.blender.org/manual/en/dev/contribute/manual/getting_started/install/index.html)** - Set up the documentation environment.
2. **[Build](https://docs.blender.org/manual/en/dev/contribute/manual/getting_started/build.html)** - Compile the manual to see changes locally.
3. **[Edit](https://docs.blender.org/manual/en/dev/contribute/manual/getting_started/editing.html)** - Make your changes.
4. **[Rebuild](https://docs.blender.org/manual/en/dev/contribute/manual/getting_started/build.html)** - Verify that your changes appear correctly.
5. **[Submit a Pull Request](https://docs.blender.org/manual/en/dev/contribute/manual/getting_started/pull_requests.html)** - Share your contributions with the community.

💡 *New to Git?*  
If you're unfamiliar with Git or pull requests, you can submit your modified file as an
[issue](https://projects.blender.org/blender/blender-manual/issues/new), and someone from the team can assist you.

## 🔗 Useful Links

- **Manual Docs:** [Contributing Guide](https://docs.blender.org/manual/en/4.4/contribute/manual/index.html)
- **Source Files:** [Manual Repository](https://projects.blender.org/blender/blender-manual)
- **Developer Forum:** [Documentation Category](https://devtalk.blender.org/c/documentation/12)
- **Chat:** [#docs on chat.blender.org](https://chat.blender.org/#/room/#docs:blender.org)
- **Administrators:** @blendify

*The Documentation Administrators manage the project, infrastructure, and contributor coordination. Feel free to reach out!*

## 📖 Join the Documentation Team

The Blender Manual is maintained by a group of contributors and administrators.
If you're interested in becoming a contributor, visit the
[developer forum](https://devtalk.blender.org/c/documentation/12) to get started.

No experience is required—just a willingness to help!

# 🌍 Translating the Blender Manual

We use Sphinx's internationalization package to support translations.
To contribute to translations, check out the [Blender Manual Translations Project](https://projects.blender.org/blender/blender-manual-translations).

## ❓ Getting Help

If you have any questions or need assistance:

- Join the **[chat](https://chat.blender.org/#/room/#docs:blender.org)** for real-time help.
- Post on the **[developer forum](https://devtalk.blender.org/c/documentation/12)** for detailed discussions.

---

We appreciate your help in making Blender's documentation better for everyone! 🚀
